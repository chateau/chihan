
# chihan

Write chinese characters, easily translate them to pinyin, get definitions and more.

## Install

Clone this directory wherever you want on your system. Then run:
```bash
cd /path/to/where-you-put/the-chihan-app/
npm install
```
This will install the necessary dependencies in `node_modules`.

Then you can start the app with `npm start` and visit your browser at `localhost:8787` or whichever other port you chose in `boot/config.js`.
