
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE HanziLookup MAIN OBJECT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var HanziLookup = {

  // MAGIC MATCHER CONSTANTS
  MAX_CHARACTER_STROKE_COUNT: 48,
  MAX_CHARACTER_SUB_STROKE_COUNT: 64,
  DEFAULT_LOOSENESS: 0.15,
  AVG_SUBSTROKE_LENGTH: 0.33, // an average length (out of 1)
  SKIP_PENALTY_MULTIPLIER: 1.75, // penalty mulitplier for skipping a stroke
  CORRECT_NUM_STROKES_BONUS: 0.1, // max multiplier bonus if characters has the correct number of strokes
  CORRECT_NUM_STROKES_CAP: 10, // characters with more strokes than this will not be multiplied

  // BASICS
  data: {},

  // GLOBAL SETTINGS
  canvasSize: 256,
  canvasStrokeWidth: 5, // width of strokes drawn on screen
  canvasStrokeColor: "white", // color of strokes drawn on screen

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ATTACH METHODS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

require("./analyze")(HanziLookup);
require("./drawingBoard")(HanziLookup);
require("./init")(HanziLookup);
require("./match")(HanziLookup);

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = HanziLookup;
