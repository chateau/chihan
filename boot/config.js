let path = require("path");

module.exports = {

  //
  //                              GENERAL

  name: "chihan", // this will overwrite the name set in package.json, you can safely omit it if you're fine with that one
  port: 8787,

  logColor: ["#57EBFF", "#DDD"],

  appPath: path.join(__dirname +"/../"), // Important, don't change this unless you move this script.

  // set to true to precalculate all dist files on server start
  production: true,

  // for more debugging logs, enable this
  // debug: true,

  // calculate list of submodules to allow to easily find less style files in submodules <false|"always"|"once">
  resolveSubmodulesPathsRecursivelyForLessImportStatments: "once",

  //
  //                              ADVANCED OPTIONS

  // unquote the following, setup the list of pages and customUrls to cache, and if you want customize pwa manifest, to enable the possibility to install your app in mobile phones and use it offline
  pwa: {
    cache: true,
    pages: [ "/", "/error/", ],
    customUrls: [
      "/lib/HanziLookupData/mmah.json",
      "/lib/HanziLookupData/orig.json",
    ],
    manifest: {
      icons: [
        {
          "src": "/share/favicon.png",
          "sizes": "560x560",
          "type": "image/png"
        },
      ],
      background_color: "#57EBFF",
    },
  },

  // NOTE: see https://framagit.org/squeak/electrode/blob/master/settings/defaultConfig.js for more custom options you can pass here
  // or have a look at electrode documentation for more information (https://squeak.eauchat.org)

  //                              ¬
  //

}
