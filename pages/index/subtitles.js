var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var hanzi = require("hanzi");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET TEXT FROM FILE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: extract text from given file
    ARGUMENTS: (
      !file <File>,
      !callback <function(text <string> « file's text »)
    )
    RETURN: <void>
  */
  getTextFromFile: function (file, callback) {
    var reader = new FileReader();
    reader.addEventListener("load", function(e) {
      callback(e.target.result);
    });
    reader.readAsText(file);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CONVERT SUBTITLES TO PINYIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: convert subtitles file to pinyin
    ARGUMENTS: (
      !text <string> « subtitles text with characters »,
      ?keepHanziLine <boolean> « if true will duplicate line adding pinyin after characters line »
    )
    RETURN: <void>
  */
  convertSubtitles2pinyin: function (text, keepHanziLine) {
    var chihan = this;

    return _.map(text.split("\n"), function (line) {
      var chars = line.split("");

      // check if line contains characters
      for (var i = 0; i < chars.length; i++) {
        var charPinyin = hanzi.getPinyin(chars[i]);
        if (charPinyin && charPinyin[0] && charPinyin[0] !== "_number") {
          var processLine = true;
          break;
        };
      };

      // return (line + converted line) | (converted line) | (line without modifications)
      if (processLine) {
        if (keepHanziLine) return line +"\n"+ chihan.convertStringToPinyin(line)
        else return chihan.convertStringToPinyin(line);
      }
      else return line;

    }).join("\n");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CONVERT SUBTITLES AND DISPLAY DOWNLOAD BUTTONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: convert the given file and display converted version download buttons
    ARGUMENTS: ( !file <File> « file to convert », )
    RETURN: <void>
  */
  convertSubtitlesAndDisplayDownloadButtons: function (file, $container) {
    var chihan = this;

    // get text from file
    chihan.getTextFromFile(file, function (text) {
      // o            >            +            #            °            ·            ^            :            |

      var outputFiles = [

        // only pinyin file
        {
          name: file.name.replace(/\.(?=[^\.]*$)/, "-pinyin."),
          text: chihan.convertSubtitles2pinyin(text)
        },

        // pinyin+hanzi file
        {
          name: file.name.replace(/\.(?=[^\.]*$)/, "-pinyin+hanzi."),
          text: chihan.convertSubtitles2pinyin(text, true)
        },

      ];

      // display download buttons
      _.each(outputFiles, function (fileobj) {
        uify.button({
          $container: $container,
          class: "download_subtitles_button",
          icomoon: "floppy-disk",
          title: fileobj.name,
          inlineTitle: "right",
          invertColor: true,
          click: function () {
            chihan.downloadSubtitlesFile(fileobj.name, fileobj.text);
          },
        });
      });

      // o            >            +            #            °            ·            ^            :            |
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SUBTITLES CONVERSION FAB
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display fab that allow to open dialog to convert subtitles files
    ARGUMENTS: ( $container <yquerjObject> )
    RETURN: <void>
  */
  displaySubtitlesConversionFab: function ($container) {
    var chihan = this;

    uify.fab({
      $container: $container,
      icomoon: "film2",
      title: "pinyinify subtitles file",
      key: "shift + s",
      size: 30,
      position: { right: "default", bottom: "default", },
      click: function () {
        uify.dialog({
          title: "Pick the file you want to convert",
          okText: "convert",
          content: function ($dialogContainer) {
            var $filesSelector = $dialogContainer.input({
              type: "file",
              multiple: "multiple",
            });
            $filesSelector.change(function () {
              _.each(this.files, function (file) {
                chihan.convertSubtitlesAndDisplayDownloadButtons(file, $dialogContainer)
              });
            });
            return false;
          },
        });
      },
    });

  },
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DOWNLOAD SUBTITLES FILE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /*
    DESCRIPTION: propose to the user to download a file with the given content
    ARGUMENTS: (
      !fileName <string> « the name to give to the file to download »,
      !text <string> « the content of the file to download »,
    )
    RETURN: <void>
    WARNING:
      ⚠️ be careful, using this function, it will open a link and has as a side effect to clear the console logs, and maybe other things
      so if you have to debug things it may be inconvenient and confusing
  */
  downloadSubtitlesFile: function (filename, text) {

    // create an <a> element with text to download as href
    var elem = document.createElement("a");
    elem.style.display = "none";
    elem.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
    elem.setAttribute("download", filename);
    // if "download" attribute is not supported by browser, open file in new tab
    if (typeof elem.download === "undefined") elem.setAttribute("target", "_blank");

    // USING THE FOLLOWING AND REMOVING ALL THE PREVIOUS elem.setAttribute IS ANOTHER WAY TO GO THAT WILL NOT REFRESH PAGE AND IT'S CONSOLE LOGS
    // however, I didn't manage to make the file naming and the content disposition work, so the only way to force it to download is to use application/octet-stream and it's all quite ugly
    // elem.setAttribute("href", "data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename%3D%22my-file.txt;charset=utf-8," + encodeURIComponent(text));


    // add it to body
    document.body.appendChild(elem);
    // click element to download the content
    elem.click();
    // remove element
    document.body.removeChild(elem);
    delete elem;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
