var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("uify");
var hanzi = require("hanzi");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// make display appropriate in mobile/touch devices
if ($$.isTouchDevice()) $("head").append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />');

// create base object
var chihan = { $el: $app.div("chihan"), };
// attach methods
$$.methods(chihan, require("./input"));
$$.methods(chihan, require("./output"));
$$.methods(chihan, require("./subtitles"));
$$.methods(chihan, require("./utilities"));

// create loading spinner
chihan.spinner = uify.spinner({ $container: chihan.$el, });

// kick app in (after timeout for spinner to be displayed)
setTimeout(function () {
  hanzi.start();
  chihan.initializeInput();
  chihan.initializeOutput();
  chihan.initializeKeyboard();
}, 10);


