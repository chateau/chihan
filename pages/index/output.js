var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var hanzi = require("hanzi");
var accentify = require("./accentify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // selection <
  //   | <integer> « index of character or pinyin to highlight and define »
  //   | <[<integer>, <integer>]> « start and end indexes of characters and pinyin to highlight and define »
  // > « the currently selected/highlighed/defined character or set of characters »

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  OUTPUT CONTAINERS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: initialize output containers and behaviours
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  initializeOutput: function () {
    var chihan = this;

    chihan.$output = chihan.$el.div("output_container");

    chihan.$output_text = chihan.$output.div("output_text");
    chihan.$output_characters = chihan.$output_text.div("characters");
    chihan.$output_pinyin = chihan.$output_text.div("pinyin");

    // pinyinify subtitles
    if (!$$.isTouchDevice()) chihan.displaySubtitlesConversionFab(chihan.$output_text);

    // + button
    uify.fab({
      $container: chihan.$output_text,
      icomoon: "plus",
      key: "shift + p",
      size: 30,
      position: { right: 60, bottom: "default", },
      click: function () {
        chihan.modifyFontSize(chihan.$output_characters, 2);
      },
    });

    // - button
    uify.fab({
      $container: chihan.$output_text,
      icomoon: "minus",
      key: "shift + m",
      size: 30,
      position: { right: 100, bottom: "default", },
      click: function () {
        chihan.modifyFontSize(chihan.$output_characters, -2);
      },
    });

    chihan.$output_definition = chihan.$output.div("output_definition");

    chihan.activateOnSelectActions(chihan);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE OUTPUT CONTENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: generate output content from the given list of characters / text
    ARGUMENTS: (
      !chars <string> « the text or series of characters to output »,
    )
    RETURN: <void>
  */
  refreshOutput: function (chars) {
    var chihan = this;

    // make output arrays
    var arrayCharacters = [];
    var arrayPinyin = [];

    // populate output arrays
    _.each(chars.split(""), function (char, charIndexInText) {

      // add to characters array
      arrayCharacters.push('<span data-char_index="'+ charIndexInText +'">'+ char +'</span>');

      // figure out pinyin for this character and add to pinyin array
      var charPinyinResult = chihan.getCharacterPinyin(char);
      arrayPinyin.push('<span data-char_index="'+ charIndexInText +'">'+ charPinyinResult +'</span>');

    });

    // output characters and pinyin
    chihan.$output_characters.htmlSanitized(arrayCharacters.join("").replace(/\n/g, "<br>"));
    chihan.$output_pinyin.htmlSanitized(arrayPinyin.join("").replace(/\n/g, "<br>"));

    // make characters and pinyin subject to hovering
    var highlightAndDefine = function () {
      chihan.selection = $(this).data("char_index");
      chihan.highlightAndDefine();
    };
    chihan.$output_characters.children().hover(highlightAndDefine).click(highlightAndDefine);
    chihan.$output_pinyin.children().hover(highlightAndDefine).click(highlightAndDefine);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  HIGHLIGHT AND DEFINED A SINGLE CHARACTER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: highlight selected div in characters and pinyin, and display definition
    ARGUMENTS: (
      ?selection <
        | <integer> « index of character or pinyin to highlight and define »
        | <[<integer>, <integer>]> « start and end indexes of characters and pinyin to highlight and define »
      > « if not defined, will use chihan.selection »,
    )
    RETURN: <void>
  */
  highlightAndDefine: function (selection) {
    var chihan = this;

    if (_.isUndefined(selection)) selection = chihan.selection;
    $(".highlight").removeClass("highlight");
    selection = _.isArray(selection) ? selection : [selection, selection];

    // highlight selection
    var characters = "";
    for (var i = selection[0]; i <= selection[1]; i++) {
      characters += chihan.highlightAndGetCharacter(i);
    };

    // display definition
    chihan.displayCharacterDefinitions(characters)

  },

  /**
    DESCRIPTION: highlight a single character and pinyin and return character text
    ARGUMENTS: ( charIndex <integer> « character index in paragraph » )
    RETURN: <string>
  */
  highlightAndGetCharacter: function (charIndex) {
    var chihan = this;

    // highlight character
    var $character = chihan.$output_characters.find("[data-char_index="+ charIndex +"]").addClass("highlight");
    // highlight pinyin
    chihan.$output_pinyin.find("[data-char_index="+ charIndex +"]").addClass("highlight");

    // return character text
    return $character.text();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DO THINGS WHEN SELECTING TEXT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: detect when text is selected in pinyin or characters, and act accordingly
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  doSomethingWithSelectedOutputText: function () {
    var chihan = this;

    // selection object and text
    var selectionObject = window.getSelection();
    var selectedText = selectionObject.toString();

    // if selection is in output left column, run selection callbacks
    if (selectedText && $(selectionObject.anchorNode).parents(".output_text").length) {

      // figure out index of first and last selected characters
      var indexFirst = $(selectionObject.anchorNode.parentNode).data("char_index");
      var indexLast = $(selectionObject.focusNode.parentNode).data("char_index");
      var indexes = [
        indexFirst < indexLast ? indexFirst : indexLast,
        indexFirst > indexLast ? indexFirst : indexLast,
      ];

      // highlight and define selection
      chihan.highlightAndDefine(indexes);

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW DEFINITION OF CHARACTER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display character definitions
    ARGUMENTS: ( selectedCharacters <string> « should be a string of characters, not pinyin » )
    RETURN: <void>
  */
  displayCharacterDefinitions: function (selectedCharacters) {
    var chihan = this;

    // get selection without spaces
    var chars = selectedCharacters.replace(/\s*/g, "");
    // clear previous definitions
    chihan.$output_definition.empty();

    //
    //                              DISPLAY CHARACTER TITLE

    chihan.$output_definition.div({
      class: "definition_title",
      text: chars,
    });

    //
    //                              DISPLAY CHARACTERS COMPONENTS

    // create decompositions container
    chihan.$output_components = chihan.$output_definition.div({
      class: "definition_components",
      text: "display components",
    }).click(function () {
      chihan.$output_components.toggleClass("mode_open");
    });

    // display decompositions
    var decomposition = hanzi.decomposeMany(chars);
    _.each(decomposition, function (charDecomposition) {

      var components = [
        charDecomposition.components1 ? charDecomposition.components1.join(", ") : undefined,
        charDecomposition.components2 ? charDecomposition.components2.join(", ") : undefined,
        charDecomposition.components3 ? charDecomposition.components3.join(", ") : undefined,
      ];

      if (components[0]) chihan.$output_components.div({ text: charDecomposition.character +": "+ components[0], });
      if (components[1] && components[1] != components[0]) chihan.$output_components.div({ text: charDecomposition.character +": "+ components[1], });
      if (components[2] && components[2] != components[1]) chihan.$output_components.div({ text: charDecomposition.character +": "+ components[2], });

    });

    //
    //                              DISPLAY DEFINITIONS

    chihan.$output_def = chihan.$output_definition.div("definition_def");
    var hanziDefinitions = hanzi.definitionLookup(chars);
    _.each(hanziDefinitions, function (defObject) {

      // display pinyin title
      chihan.$output_def.div({
        class: "definition_deftitle",
        text: _.map(defObject.pinyin.split(" "), accentify).join(" "),
      });

      // display definition
      chihan.$output_def.div({
        class: "definition_defcontent",
        htmlSanitized: defObject.definition.split("/").join("<br>"),
      });

    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTIVATED DOING THINGS ON SELECTING TEXT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: kick in action to be triggered when text is selected in the output
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  activateOnSelectActions: function () {
    var chihan = this;

    document.onmouseup = function () { chihan.doSomethingWithSelectedOutputText(); };
    document.onkeyup =   function () { chihan.doSomethingWithSelectedOutputText(); };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
