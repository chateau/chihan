var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("uify");
var HanziLookup = require("../../global/HanziLookup");
var requireElec = require("electrode/lib/require");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE INPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: initialize input containers and behaviours
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  initializeInput: function () {
    var chihan = this;

    // create inputs main container
    chihan.$inputContainer = chihan.$el.div("input_container");

    // initialize drawing board and textinput sections
    chihan.initializeDrawingBoard();
    chihan.initializeTextinput();

    // create fab to toggle inputs display (for phones only)
    if ($$.isTouchDevice()) {
      uify.fab({
        $container: chihan.$el,
        class: "mobile-input_dialog_display_fab",
        icomoon: "pencil",
        position: { top: "stick", left: "stick", },
        click: function () {
          chihan.$inputContainer.toggleClass("hidden")
        },
      });
      chihan.$inputContainer.addClass("hidden");
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TRIGGER INPUTS DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: trigger inputs displaying
    ARGUMENTS: ( !$container <yquerjObject> « element in which to create inputs container », )
    RETURN: <void>
  */
  displayInputs: function ($container) {
    var chihan = this;


  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE DRAWING BOARD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: initialize drawing board section
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  initializeDrawingBoard: function () {
    var chihan = this;

    // create drawing board main container
    chihan.$input_drawing = chihan.$inputContainer.div("input_drawing");

    // drawing board
    chihan.$drawingBoard = chihan.$input_drawing.div("drawingboard");

    // board buttons container
    chihan.$drawingBoardButtons = chihan.$input_drawing.div("drawingboard_buttons");

    // clear button
    uify.button({
      $container: chihan.$drawingBoardButtons,
      title: "clear",
      inlineTitle: "right",
      key: "shift + backspace",
      click: function () {
        chihan.drawingBoard.clearCanvas();
        chihan.drawingBoard.redraw();
        chihan.proposeCharacters();
      },
    });

    // undo button
    uify.button({
      $container: chihan.$drawingBoardButtons,
      title: "undo",
      inlineTitle: "right",
      key: "backspace",
      click: function () {
        chihan.drawingBoard.undoStroke();
        chihan.drawingBoard.redraw();
        chihan.proposeCharacters();
      },
    });

    // space button
    uify.button({
      $container: chihan.$drawingBoardButtons,
      title: "space",
      inlineTitle: "right",
      key: "spacebar",
      click: function () {
        var newValue = (chihan.$textinput.val() || "") + " ";
        chihan.modifyTextinput(newValue);
      },
    });

    // proposed characters
    chihan.$proposedCharacters = chihan.$input_drawing.div("recognized_characters");
    chihan.$proposedCharacters_hanziLookupChars = chihan.$proposedCharacters.div();
    chihan.$proposedCharacters_mmahLookupChars = chihan.$proposedCharacters.div();

    // initialize hanzilookup library
    chihan.initializeHanzilookup();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE TEXTINPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: initialize textinput section
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  initializeTextinput: function () {
    var chihan = this;

    // create textinput main container
    chihan.$input_text = chihan.$inputContainer.div("input_text");

    // create textinput
    chihan.$textinput = chihan.$input_text.textarea("textinput");
    chihan.$textinput.select(function (event) {
      chihan.selection = [event.currentTarget.selectionStart, event.currentTarget.selectionEnd - 1];
      chihan.highlightAndDefine();
    });
    chihan.$textinput.bind("input propertychange", function () {
      chihan.refreshOutput(chihan.$textinput.val());
    });

    // textinput buttons container
    chihan.$textinputButtons = chihan.$input_text.div("textinput_buttons");

    // clear button
    uify.button({
      $container: chihan.$textinputButtons,
      title: "clear",
      inlineTitle: "right",
      click: function () {
        chihan.modifyTextinput("");
      },
    });

    // + button
    uify.button({
      $container: chihan.$textinputButtons,
      title: "+",
      inlineTitle: "right",
      click: function () {
        chihan.modifyFontSize(chihan.$textinput, 2);
      },
    });

    // - button
    uify.button({
      $container: chihan.$textinputButtons,
      title: "-",
      inlineTitle: "right",
      click: function () {
        chihan.modifyFontSize(chihan.$textinput, -2);
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MODIFY TEXTINPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  modifyTextinput: function (newValue) {
    var chihan = this;

    // set value in textarea
    chihan.$textinput.val(newValue);

    // refresh output as well, since the change event doesn't seem to detect it
    chihan.refreshOutput(newValue);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE HANZI LOOKUP LIBRARY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: load hanzilookup dependency and prepare drawing board
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  initializeHanzilookup: function () {
    var chihan = this;

    // change canvas size depending on device
    HanziLookup.canvasSize = 288; // == 20em

    var _filesToLoad;

    //
    //                              LOAD JSON DATA WHEN PAGE IS READY, BECAUSE THEY'RE LARGE AND IT TAKES LONG

    $(document).ready(function () {
      _filesToLoad = 2;
      HanziLookup.init("mmah", "/lib/HanziLookupData/mmah.json", fileLoaded);
      HanziLookup.init("orig", "/lib/HanziLookupData/orig.json", fileLoaded);
    });

    //
    //                              INITIALIZE DRAWING BOARD ONCE ALL FILE HAVE BEEN LOADED

    function fileLoaded (success) {

      // error loading file
      if (!success) {
        _filesToLoad = -1;
        $$.log.error("Failed to load a hanzilookup file: ", success);
        uify.toast.error("Failed to load a hanzilookup file, see logs for details.");
        return;
      };

      // continue until all files loaded
      --_filesToLoad;
      if (_filesToLoad != 0) return;

      // remove spinner when all is loaded
       chihan.spinner.destroy();

      // create handwriting canvas
      chihan.drawingBoard = HanziLookup.DrawingBoard(
        chihan.$drawingBoard.first(),
        chihan.proposeCharacters
      );

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOOKUP CHARACTERS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: fetches hand-drawn input from drawing board and looks up Hanzi
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  proposeCharacters: function () {
    var chihan = this;

    // decompose character from drawing board
    var analyzedChar = new HanziLookup.AnalyzedCharacter(chihan.drawingBoard.cloneStrokes());

    // look up with original HanziLookup data
    var matcherOrig = new HanziLookup.Matcher("orig");
    matcherOrig.match(analyzedChar, 8, function (matches) {
      chihan.showResultingCharacters(chihan.$proposedCharacters_hanziLookupChars, matches);
    });

    // look up with MMAH data
    var matcherMMAH = new HanziLookup.Matcher("mmah");
    matcherMMAH.match(analyzedChar, 8, function (matches) {
      chihan.showResultingCharacters(chihan.$proposedCharacters_mmahLookupChars, matches);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW CHARACTER RESULTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: populates UI with (ordered) Hanzi matches
    ARGUMENTS: (
      !$container <yquerjObject>,
      !matches <{ character: <string>, score: <number> }[]>,
    )
    RETURN: <void>
  */
  showResultingCharacters: function ($container, matches) {
    var chihan = this;

    $container.empty();

    _.each(matches, function (char) {
      $container.span({ text: char.character, }).click(function () {
        var newValue = (chihan.$textinput.val() || "") + char.character;
        chihan.modifyTextinput(newValue);
      });
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
