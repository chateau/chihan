var _ = require("underscore");
var $$ = require("squeak");

var toneConversion = {
  "a": [ "a", "ā", "á", "ǎ", "à", "a" ],
  "e": [ "e", "ē", "é", "ě", "è", "e" ],
  "i": [ "i", "ī", "í", "ǐ", "ì", "i" ],
  "o": [ "o", "ō", "ó", "ǒ", "ò", "o" ],
  "u": [ "u", "ū", "ú", "ǔ", "ù", "u" ],
  "ü": [ "ü", "ǖ", "ǘ", "ǚ", "ǜ", "u" ]
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: convert numbered pinyin to accentified pinyin (for example converts "ni3" to "nǐ")
  ARGUMENTS: (
    ?syllable <string> « for example ni3 »,
    ?verbose <boolean> « if true, will display error messages on inappropriate syllables »;
  )
  RETURN: <string>
*/
module.exports = function (syllable, verbose) {

  // syllable should be a string
  if (!_.isString(syllable)) {
    if (verbose) $$.log.warning("Syllable is not a string: ", syllable);
    return syllable;
  };

  // find letter to toneify
  if (toneConversion[syllable[0]]) var letterToToneify = syllable[0]
  else if (toneConversion[syllable[1]]) var letterToToneify = syllable[1]
  else if (toneConversion[syllable[2]]) var letterToToneify = syllable[2]
  else {
    if (verbose) $$.log.error("Could not identify letter to toneify: ", syllable);
    return syllable;
  };

  // find requested tone
  var toneNumber = +_.last(syllable);
  if (_.isNaN(toneNumber)) {
    $$.log.error("Could not figure out tone number: ", syllable);
    return syllable;
  };

  // return modified syllable
  return syllable.replace(letterToToneify, toneConversion[letterToToneify][toneNumber]).replace(/\d$/, "");

};
