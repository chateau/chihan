var _ = require("underscore");
var $$ = require("squeak");
var keyboardify = require("keyboardify");
var hanzi = require("hanzi");
var accentify = require("./accentify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INCREASE FONT SIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: modify the given element's font size
    ARGUMENTS: (
      !$elem <yquerjObject>,
      !amount <number> « number of pixels increase or decrease »,
    )
    RETURN: <void>
  */
  modifyFontSize: function ($elem, amount) {
    var fontSize = +$elem.css("font-size").replace(/px$/, "");
    $elem.css("font-size", fontSize + amount);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE KEYBOARD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  initializeKeyboard: function () {
    var chihan = this;

    keyboardify.bind("left", function () {
      if (_.isArray(chihan.selection)) chihan.selection = chihan.selection[0] - 1
      else if (chihan.selection) chihan.selection--
      else chihan.selection = 0;
      chihan.highlightAndDefine();
    });

    keyboardify.bind("right", function () {
      if (_.isArray(chihan.selection)) chihan.selection = chihan.selection[1] + 1
      else if (
        _.isNumber(chihan.selection) &&
        chihan.selection < chihan.$output_characters.children().length - 1
      ) chihan.selection++
      else chihan.selection = chihan.$output_characters.children().length - 1;
      chihan.highlightAndDefine();
    });

    keyboardify.bind("shift + left", function () {
      if (_.isArray(chihan.selection)) chihan.selection = [chihan.selection[0] - 1, chihan.selection[1]]
      else if (chihan.selection) chihan.selection = [chihan.selection - 1, chihan.selection]
      else chihan.selection = 0;
      chihan.highlightAndDefine();
    });

    keyboardify.bind("shift + right", function () {
      if (_.isArray(chihan.selection)) chihan.selection = [chihan.selection[0], chihan.selection[1] + 1]
      else if (
        _.isNumber(chihan.selection) &&
        chihan.selection < chihan.$output_characters.children().length - 1
      ) chihan.selection = [chihan.selection, chihan.selection + 1]
      else chihan.selection = chihan.$output_characters.children().length - 1;
      chihan.highlightAndDefine();
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CONVERT CHARACTER(S) TO PINYIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: convert a single character to pinyin
    ARGUMENTS: ( char <string> « single character to convert to pinyin », )
    RETURN: <string> « pinyin for the given character »
  */
  getCharacterPinyin: function (char) {
    var charPinyin = hanzi.getPinyin(char);
    var charPinyinResultWithNumber = charPinyin && charPinyin[0] && charPinyin[0] !== "_number" ? charPinyin[0] : char;
    return accentify(charPinyinResultWithNumber);
  },

  /**
    DESCRIPTION: convert the given string of characters to pinyin
    ARGUMENTS: ( chars <string> )
    RETURN: <string> « pinyin for the given string »
  */
  convertStringToPinyin: function (chars) {
    var chihan = this;

    return _.map(chars, function (char, index) {
      var charPinyin = hanzi.getPinyin(char);
      if (charPinyin && charPinyin[0] && charPinyin[0] !== "_number") {
        var resultPinyin = accentify(charPinyin[0]);
        return (index ? " " : "") + resultPinyin;
      }
      else return char;
    }).join("");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
